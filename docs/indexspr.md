# Snowpack Reader

This modile is meant as a reader and handler for .pro files. 

## Usage

.pro files are read with the readPRO function, returning an Object containing all the information about 
the file, as well as the data.

The recommended way of accessing data are the getter functions.

A profile on a specific date is stored as a Snowpack object, used to handle the profiles properly.
The main way of using the data is the toDf() method, which returns a pandas dataframe.
It allows to rename the colum labels by using a dictionary where the current labels are mapped to new ones, given to the method.

It is also possible to only get data for a specific param, from the Snowpack object, or from the Reader for all dates.

## Examples

```python
import snowpat.snowpackreader as spr
pro = spr.readPRO("test.pro")

# print a summary of the file
print(pro)

station_name = pro.metadata["station_name"]

# all available dates
dates = pro.get_all_dates()

# will only return data above the ground after this
pro.discard_below_ground(True)
# get a Snowpack object (internal data class for Profiles) on a specific date
profile = pro.get_profile_on(dates[0])
profile2 = pro.get_profile_nr(200) # easy acces to the xth profile

# convert it to a dataframe with minimum stability and surface hoar as metadata
# column names will be data codes, except for "0500"= height (layer boundaries)-> 2 columns: layer middle and layer thickness
profile.toDf().head()
wl = profile.weak_layer
sh = profile.surface_hoar

# There is help, to deal with the DataCodes:
# per default, the names are as in the .pro Header (without units)
pro.update_name_of_code("0503", "Snow Density")
density_code = pro.name_to_code("Snow Density")

#convert to a dataframe, that has wl and sh integrated into the table
profile.toDf(integrate=True).head()

profile.toDf(CodesToName=pro.DataCodes).head()

# get the original values of data code "0500" = height
layer_boundaries = profile.layer_boundaries

# get a list of profiles at all available dates
list_of_profiles = pro.get_all_profiles()

# write to a different file formate
# same integrate as above
pro.toCSV("out_file.csv", integrate = False)  # can get quite big
pro.toHDF5("out_file.h5", integrate=False) # recommended, much quicker and more robust

metadata, list_of_profiles = readCSV("out_file.csv") # can take very long
metadata, list_of_profiles = readHDF5("out_file.h5") # recommended, very fast

```