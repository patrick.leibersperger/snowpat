
# SNOWPAT

This library is used to handle file formats widely used in Snow Science

## Installation

The recommended way of installing this library is via the PyPi library:

```bash
pip install [--user] snowpat
```

## Modules

Extensive Documentation on the Modules can be found in the Docs on the Top.

For reading, writing and handling iCSV files, see [icsv](indexicsv.md)

For reading, writing and handling SMET files, see: [pysmet](indexsmet.md)

The SNOWPACK legacy output files .pro can be read with: [snowpackreader](indexspr.md)

And a plotting Framework for Snowpacks and Profiles is available under: [snowlense](indexplot.md) 
