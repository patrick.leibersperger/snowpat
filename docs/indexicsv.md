# icsv

This is a module for reading, writing, and handling iCSV files.

## Usage

An iCSV file is read with read(filename), which returns an iCSVFile object (module specific class).
It can also be Read via icsv.iCSVFile(filename).

A sanity check for the Metadata and Fields is performed before writing and after reading.

metadata contains all the information in the METADATA section, fields contains information on the FIELDS section, i.e. all values will be 
lists the length of the number of columns.

### Examples

#### Reading

```python
import snowpat.icsv

file = icsv.read(filename)
data_pandas = file.data
data_xarray = file.to_xarray()
field_delimiter = file.metadata.get_attribute("field_delimiter")
fields = file.fields.get_attribute("fields")

# other possible metadata is accessed the same, but will return None if it is not present
station_id = file.metadata.get_attribute("station_id")

# changing metadata
file.metadata = file.metadata.set_attribute("field_delimiter", "|")

# and for writing to an output again (if no output filename is provided, the given filename is used with an out flag):
file.write(out_filename)

# a summary is also available wih
file.info()

# For converting smet files to icsv use:
smet_file = smet.read(smet_filename)
icsv_file = from_smet(smet_file)
```

#### Writing

It is also possible to create SMET file, however no checks on the input value is performed, so this is the users responsibility.

```python
import pysmet as icsv
import pandas as pd

# create a file object, with a filename, and a flag, to not read from a file
file = icsv.iCSVFile() 

# create some example data, can of course also be read from CSV...
data_pd = pd.DataFrame(data, columns=["T", "RH", "P"])

# will use the given column names as fields list
file.setData(data_pd) 

data_p = pd.DataFrame(data)
# will not work, as no column names are provided.
file.setData(data_p) 
# you can give a list of column names as well:
file.setData(data_p, ["T","RH","P"])

# set the metadata ( needs at least the required metadata, as a sanity check will be performed before writing)
# fields will already be set, if you followed the examples above
file.metadata.set_attribute("field_delimiter",":")
file.metadata.set_attribute("geometry","POINT(1 1)")
file.metadata.set_attribute("srid","EPSG:1234")


file.info()
file.write(out_filename)

```

## License

This project is licensed under the terms of the GNU-GPL-3.0 license.