# PySMET

This is a module for reading, writing, and handling SMET files. 

## Usage

A SMET file is read with read(filename), which returns a SMETFile object (module specific class). 
It can also be Read via pysmet.SMETFile(filename,read=true,num_header_lines=number_of_header_lines) (including "[DATA]" as header line). 

### Examples

#### Reading

```python
import snowpat.pysmet as smet

file = smet.read(filename)
data_pandas = file.data
data_numpy = file.toNumpy()
station_id = file.meta_data.station_id
lon = file.meta_data.location.longitude
acdd_creator_name = file.acdd_meta_data.get_attribute("creator_name")

# changing metadata
file.meta_data.station_id = "WFJ"
file.acdd_meta_data.set_attribute("creator_name", "SomeName")

# and for writing to an output again (if no output filename is provided, the given filename is used with an out flag):
file.write(out_filename)

# a summary is also available wih
file.info()
```

#### Writing

It is also possible to create SMET file, however no checks on the input value is performed, so this is the users responsibility.

```python
import pysmet as smet
import numpy as np
import pandas as pd

# create a file object, with a filename, and a flag, to not read from a file
file = smet.SMETFile(filename, read=False) 

# create some example data, can of course also be read from CSV...
data_pd = pd.DataFrame(data, columns=["T", "RH", "P"])
# will use the given column names as fields list
file.setData(data_pd) 

data_p = pd.DataFrame(data)
# will ask you to set fields manually, as no column names are provided
file.setData(data_p) 

data = np.random.rand(100, 3)
 # set the data from a  numpy array, needs the column names
file.fromNumpy(data, ["timestamp", "temperature", "humidity"])

# set the metadata

# set identifier with a different version, not needed if using the default
file.setIdentifier(version="7353")
# set all the necessary Metadata (is specified in the doc of setMetaData)
file.setMetaData("station_id", "FOO")
file.setMetaData("location", smet.locFromLatLon(45, 8))
file.setMetaData("nodata", -999)
# not needed, if set by fromNumpy with column names, or if given dataframe had meaningful names
file.setMetaData("fields", ["time", "temperature", "humidity"])
# setMetaData supports adding any key to acdd metadata by using "acdd_..."
file.setMetaData("acdd_creator_name", "SomeName")
# and directly using a supported acdd key
file.setMetaData("creator_email", "SomeEmail")

file.info()
file.write()

```

## License

This project is licensed under the terms of the GNU-GPL-3.0 license.